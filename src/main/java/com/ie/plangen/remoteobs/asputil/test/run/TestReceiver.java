package com.ie.plangen.remoteobs.asputil.test.run;

import org.apache.commons.configuration2.ex.ConfigurationException;

import com.ie.plangen.remoteobs.asputil.amq.AmqReceiver;

import javax.jms.*;

public class TestReceiver {
    public static void main(String[] args) {
		AmqReceiver ar = null;
		try {
			ar = new AmqReceiver();
		} catch (ConfigurationException e) {
			e.printStackTrace();
		} catch (JMSException e) {
			e.printStackTrace();
		}
			ar.runReceiver();
    }
}
package com.ie.plangen.remoteobs.asputil.plangen.plantarget;

import com.ie.plangen.remoteobs.asputil.plangen.PlanCheckExeption;
import com.ie.plangen.remoteobs.asputil.plangen.PlanFileContent;

public abstract class PlanTarget {
	

	private String outString;

	public String getOutString() {
		return outString;
	}

	public void setOutString(String outString) {
		this.outString = outString;
	}
	
	public String toOutString() throws PlanCheckExeption {
		return outString+PlanFileContent.NewLine;
	}
	
	public int[] getCountArray() {
		return countArray;
	}

	public double[] getCountInterval() {
		return countInterval;
	}

	public String[] getCountFilter() {
		return countFilter;
	}

	public int[] getCountBinning() {
		return countBinning;
	}

	protected int [] countArray={};
	protected double[] countInterval={};
	protected String[] countFilter={};
	protected int [] countBinning={};
	
}
